from PIL import Image
import os
import re
import time

dir_path = ''
out_name = ''

def getAllFile (filePath):
  files = os.listdir(filePath)
  return files

def handleConversion (fileName):
  global dir_path
  global out_name
  filePath = f'{dir_path}\\{fileName}.webp'
  outName = f'{out_name}\\{fileName}.gif'
  im = Image.open(filePath)
  # 保存前将“背景”设置为“无”
  im.info.pop('background', None) 
  im.save(outName, 'gif', save_all=True)
  print(f"{fileName}.gif 成功..")

def get_time():
    t = time.gmtime()
    return time.strftime("%Y-%m-%d %H-%M-%S", t)

def start():
  global dir_path
  global out_name
  while True:
    dir_path = input("输入webp图片目录：")
    if dir_path == '':
      continue
    dir_path = dir_path.replace('/', '\\')
    if dir_path[-1] == '\\':
      dir_path = dir_path[:-1]
    break
  while True:
    out_name = input('输入保存的当前目录的文件夹名[默认：images+当前时间]：')
    if out_name == "":
      out_name = f'images {get_time()}'
    if not os.path.isdir(out_name):
      os.mkdir(out_name)
      break
  files = getAllFile(dir_path)
  for file in files:
    # webp格式且未转过的才需要转
    if re.findall(r'webp$', file) != []:
      [fileName, ext] = file.split('.')
      handleConversion(fileName)

if __name__ == '__main__':
  start()